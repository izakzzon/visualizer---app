from pyproj import Proj, transform, CRS, Transformer
import ujson

with open('isis.json') as file:  
    test = ujson.load(file)

for row in test:
    loc = row["location"]
    p1 = Proj(init='epsg:3095', preserve_units=True) #EPSG:3095 Tokyo / UTM zone 54N
    p2 = Proj(init='epsg:4326')
    x1, y1 = p1(float(loc["lng"]),float(loc["lat"]))
    x2, y2 = transform(p1,p2,x1,y1)
    loc["lng"] = x2
    loc["lat"] = y2
    
with open('isis2.json', 'w') as file2:  
    ujson.dump(test,file2)


