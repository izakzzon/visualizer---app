var express = require('express');
var router = express.Router();
const fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('result.json', 'utf-8'));
  console.log(content);
  res.jsonp(content);
});

router.get('/ud', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('result.json', 'utf-8'));
  res.jsonp(content);
});

router.get('/isuzu', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('isuzu.json', 'utf-8'));
  res.jsonp(content);
});

router.get('/hino', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('hinoraw.json', 'utf-8'));
  res.jsonp(content);
});

router.get('/fuso', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('fuso.json', 'utf-8'));
  res.jsonp(content);
});

router.get('/geo', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('./land/japan.geojson', 'utf-8'));
  console.log(content);
  res.jsonp(content);
});


module.exports = router;
