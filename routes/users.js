var express = require('express');
var router = express.Router();
const fs = require('fs');
/* GET users listing. */
router.get('/', function(req, res, next) {
  let content = JSON.parse(fs.readFileSync('result.json', 'utf-8'));
  console.log(content);
  res.jsonp(content);
});

module.exports = router;
